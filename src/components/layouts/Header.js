import React from 'react';
import {AppBar, Toolbar} from 'material-ui';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Button from '@material-ui/core/Button';
import UserAvatar from '../user-states/UserAvatar';
import UserMenu from '../user-states/UserMenu';

const muiTheme = getMuiTheme({
  palette: {
    textColor: "#ffffff",
  },
  appBar: {
    height: '100%',
    width: '100%',
  },
});

const styles = {
  Tabs: {
    marginLeft: '10%',
    backgroundColor: "#00bcd4"
  },
  
  ToolBar: {
    backgroundColor: "#00bcd4"
  },
  
  Typography: {
    left:"10%",

  },
  
  AppBar: {
    height: "100%",
    position: 'relative',

  },

  loginButton: {
    left: "10%",
  },
}

class TopBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      signedIn: false 
    };
    this.handleUserLogin = this.handleUserLogin.bind(this);
  }

  handleUserLogin () {
    this.setState({
      signedIn: !this.state.signedIn
    })
  }  

  render() {
    const { classes } = this.props;

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <AppBar style={styles.AppBar}>
          <Toolbar style={styles.ToolBar}>
            <div>
              <img src={this.props.logoImage} alt="SZHR Logo" />
            </div>
            <div style={{marginLeft: "15%"}}>
              {this.props.navigationComponents}
            </div>
            <div style={{marginLeft: "5%"}}>
              {this.state.signedIn ?
                <UserMenu updateUserLoginState={this.handleUserLogin}>
                  <UserAvatar avatarText="S" /> 
                </UserMenu>:
                <Button 
                  color="inherit" 
                  style={styles.loginButton}
                  onClick={this.handleUserLogin.bind(this)}
                >
                  Login
                </Button>
              }
            </div>
          </Toolbar>      
        </AppBar>
      </MuiThemeProvider>

    );
  }
}

TopBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TopBar);






