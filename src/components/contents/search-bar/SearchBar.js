import React, {Fragment} from 'react';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import Icon from '@material-ui/core/Icon';
import search_icon from '../../../images/icons/search-icon.svg';

const styles = {

  Container: {
    position: 'absolute',
    height: '60px',
    width: '48%',
    left: '380px',
    top: '150px',

  },

  gridTile: {
    width: '300px'
  },

  searchTextField: {
    // left:"100px",
    width: '500px'
  },
  searchTextBoxContainer: {
    position: 'relative',
    left:"550px",
    top: '138px',
    width: '500px',
  },
  selectContainer: {
    position: 'relative',
    width: "50px",
    height: '55px',
    left: '500px',
    top: '190px',
    // align: "center",
  }


};


export default props =>
  <Paper style={styles.Container}>
    <Select value="Select City">
      <MenuItem value="Select City">
        <em>Select City</em>
      </MenuItem>
      <MenuItem value={10}>New York</MenuItem>
      <MenuItem value={20}>Philadelphia</MenuItem>
      <MenuItem value={30}>Boston</MenuItem>
    </Select>
    <TextField
      id="full-width"
      InputLabelProps={{
        shrink: true,
      }}
      placeholder="Job titles, Keywords, or Company"
      // fullWidth
      margin="normal"
      style={styles.searchTextField}
    />
    <Button variant="contained" color="secondary" size='small'>
      <img src={search_icon} alt="search" />
    </Button>
  </Paper>