import React, {Component, Fragment} from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import AddIcon from '@material-ui/icons/Add';
import Icon from '@material-ui/core/Icon';
import DeleteIcon from '@material-ui/icons/Delete';
import NavigationIcon from '@material-ui/icons/Navigation';


const styles = theme => ({
  FormControl: {
    width: 500
  }
});


export default withStyles(styles) (class extends Component {
  state = {
    open: false,
    exercise: {
      title: "",
      description: "",
      muscles: ""
    }
  }

  handleToggle = () => {
    this.setState({
      open: !this.state.open
    })
  }

  handleChange = name => ({target: {value}}) => {
    this.setState({
      exercise: {
        ...this.state.exercise, 
        [name]: value     
      }
    })
  }

  handleSubmit = () => {
    // To-do: validate
  }

  render() {
    const {open, exercise: {title, description, muscles}} = this.state,
          {classes, muscles: categories} = this.props

    return (
      <Fragment>
        <Button variant="fab" onClick={this.handleToggle} mini>
          <AddIcon />
        </Button>
        <Dialog
          open={open}
          onClose={this.handleToggle}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">
            Create a New Exercise
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              Please fill out the form below.
            </DialogContentText>
            <form>
              <TextField
                label="Title"
                value={title}
                onChange={this.handleChange('title')}
                margin="normal"
                className={classes.FormControl}
              />
              <br />
              <FormControl className={classes.FormControl}>
                <InputLabel htmlFor="muscles">Muscles</InputLabel>
                <Select
                  value={muscles}
                  onChange={this.handleChange('muscles')}
                >
                  {categories.map(category =>
                    <MenuItem value={category}>
                      {category}
                    </MenuItem>
                  )}
                </Select>
              </FormControl>              
              <TextField
                label="Description"
                value={description}
                multiline
                rows='4'
                onChange={this.handleChange('description')}
                margin="normal"
                className={classes.FormControl}
              />       
            </form>
          </DialogContent>
          <DialogActions>
            <Button 
              onClick={this.handleSubmit} 
              color="primary"
              variant='raised'
            >
              Submit
            </Button>
          </DialogActions>
        </Dialog>  
      </Fragment>   
    ); 
  }
})
