import React, {Component, Fragment} from 'react';
import {Header, Footer} from './layouts';
import TopBar from './layouts/Header';
import Contents from './contents'
import szhrLogo from '../images/icons/szhr-logo-small.jpeg'
import NavigationTab from './navigation/NavTab'

const navTabTitles = [
  "Home", "Job Search", "Recruitment Program", "Events", "About us"
]

const styles = {

}

export default class extends Component {
  render() {
    return (
      <Fragment>
        <div>
          <TopBar
            logoImage={szhrLogo}
            navigationComponents={
              <NavigationTab
                tabTextList={navTabTitles}
              />
            }
          />
        </div>
        <Contents />
      </Fragment>
    );
  }
}