import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const styles = {
  root: {
    flexGrow: 1,
    backgroundColor: "#00bcd4",
    borderWidth: 0,
    boxShadow: 'none',
  },
};

class NavigationTab extends React.Component {
  state = {
    value: 0,
  };

  handleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { classes } = this.props;
    const tabTextList = this.props.tabTextList;

    return (
      <Paper className={classes.root}>
        <Tabs
          value={this.state.value}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          {tabTextList.map(itemText => (
            <Tab label={itemText} />
          ))}
        </Tabs>
      </Paper>
    );
  }
}

NavigationTab.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(NavigationTab);