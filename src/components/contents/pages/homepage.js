import React, {Fragment} from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';

import Image from '../../../images/backgrounds/manhattan-night.jpg';
import Button from '@material-ui/core/Button';

const styles = {
  Background: {
    height: 800,
    width: 1424,
    backgroundImage: `url(${Image})`,
    marginTop: '1px'
  },

  bannerContainer: {
    // top: "300px",
    paddingTop: '200px',
    width: 1425,
  },

  welcomeBanner: {
    width: 1425,
    backgroundColor: 'transparent',
  },

  Typography:{ 
    position: 'relative',
    left: '250px',
    color: 'white'
  },

  SignIn: {
    postion: "relative",
    height: 50,    
    width: 120,
    left: '10px',
    color: 'white',
    // backgroundColor: 'black'

  },

  SignUp: {
    postion: "relative",
    height: 50,    
    width: 120,
    left: '570px',
    color: 'white',
    // backgroundColor: 'black'

  },


  ButtonTileBar: {
    position: 'relative',
    width: 1430,
    height: 50,
    align: 'center',
    left: '200',
  }
};


export default props =>
  <Fragment>
    <Paper 
      style={styles.Background} 
      variant="headline" 
      component="h3"
      square='false'
    >
      <GridList cellHeight={400}>
        <GridListTile style={styles.bannerContainer}>
          <Typography variant='display2' style={styles.Typography}>
            Welcome to SZHR Overseas Talents Program
          </Typography>
        </GridListTile>
      </GridList>
      <GridList cellHeight='auto'>
        <GridListTile>
          <Button 
            variant="contained" 
            color="primary" 
            style={styles.SignUp}
            size="large"
          >
            Sign up
          </Button>
        </GridListTile>
        <GridListTile >
          <Button 
            variant="contained" 
            color="primary" 
            style={styles.SignIn}
            size="large"
          >
            Sign in
          </Button>
        </GridListTile>
      </GridList>  
    </Paper>

  </Fragment>




