import React from 'react';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import szhr_logo from '../../images/icons/szhr-logo.jpeg';

const styles = {
  FooterContainer: {
    marginTop: '0px',
    height: '80px'
  },

  Logo: {
    position: 'relative',
    left: '280px',
    width: '80px',
    height: '70px'
  },

  footInfoContainer: {
    border: 'none',
  },

  contactInfoContainer: {
    position: 'relative',
    left: '50px',
    width: '400px',
    height: '70px',
  }

};


export default props => 
  <Paper style={styles.FooterContainer}>
    <Grid container spacing={16}>
      <Grid item lg>
        <Grid container justify="left" spacing='0' style={styles.contactInfoContainer}>
          <Paper style={styles.footerInfoContainer}>
            <Typography variant='title' style={{}}>
              Contact Us
            </Typography>
            <br/>
            <Typography variant='title' style={{width: "300px"}}>
              Wechat: wxszhr
            </Typography>
          </Paper>
        </Grid>
      </Grid>
      <Grid item lg>
        <Grid container justify="right" spacing='0' style={styles.Logo}>
          <Paper style={styles.footerInfoContainer}>
            <img src={szhr_logo} alt="szhr_logo" />
          </Paper>
        </Grid>
      </Grid>
    </Grid>
  </Paper>


