import React, {Fragment} from 'react';
import Paper from '@material-ui/core/Paper';
import FeedCard from './cards/FeedCard';
import NewsCard from './cards/NewsCard'
import CardList from './cards/CardList';
import logo from "../../images/demo_sponsors/tesla.png";
import FilterBar from './select-bar/FilterBar'
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = {
  cardList: {
    width:"100%",
  },
  userPageContainer: {
    marginTop: 5,
    height: '100%',
    maxHeight: "1000px",
    backgroundColor: '#efefef'
  },
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 100,
  },
  // control: {
  //   padding: theme.spacing.unit * 2,
  // },
};


export default props =>

  <Fragment>
    <Paper 
      style={styles.userPageContainer} 
      variant="headline" 
      component="h3"
      square='false'
    >

      <Grid container className={styles.root} spacing={2}>
        <Grid item xs={12}>
          <Grid container className={styles.demo} justify="center" spacing={2}>
            <Grid item md>
              <NewsCard 
                title="What's Popular"
                postContent={Array(3).fill().map((_, i) => i).map(itemId => (
                  <ListItem key={`item-${itemId}`}>
                    <ListItemText primary="Test Text" />
                  </ListItem>
                ))}
                collapsedContent={Array(2).fill().map((_, i) => i).map(itemId => (
                  <ListItem key={`item-${itemId}`}>
                    <ListItemText primary="Collapsed Text" />
                  </ListItem>
                ))}
                style={{width: "10%", left:"20%"}}
              />
            </Grid>
            <Grid item lg>
              <CardList 
                numItem={10}
              >
                  <FeedCard 
                    avatarText="A"
                    title="User"
                    postTimeString="2018/08/02"
                    imgPath={logo}
                    imgTitle="Sponsor"
                    postContent="We are hiring"
                  />
              </CardList>
            </Grid>
            <Grid item md>
              <NewsCard 
                title="What's Popular"
                postContent={Array(3).fill().map((_, i) => i).map(itemId => (
                  <ListItem key={`item-${itemId}`}>
                    <ListItemText primary="Test Text" />
                  </ListItem>
                ))}
                collapsedContent={Array(2).fill().map((_, i) => i).map(itemId => (
                  <ListItem key={`item-${itemId}`}>
                    <ListItemText primary="Collapsed Text" />
                  </ListItem>
                ))}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Paper>
  </Fragment>




