import React, {Fragment} from 'react';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import SearchBar from './search-bar/SearchBar'
import user_background from '../../images/backgrounds/user_background.jpg'
import google_logo from '../../images/demo_sponsors/google.png'
import facebook_logo from '../../images/demo_sponsors/facebook_logo.png'
import renaissance_logo from '../../images/demo_sponsors/renaissance.jpeg'
import amazon_logo from '../../images/demo_sponsors/amazon.png'
import tesla_logo from '../../images/demo_sponsors/tesla.png'

import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';

const styles = {
  userPageContainer: {
    height: "800px",
    // backgroundImage: `url(${user_background})`,

  },

  searchBar: {
    top: "40%",
    left: "40%",
  },

  searchBarContainer: {
    // position: 'relative',
    height: "513px",
    width: '1425px',
    backgroundImage: `url(${user_background})`,
  },

  sponsorTitle:{
    position: 'relative',
    color:"#7a7a7a", 
    left: "600px",
    top: "20px",

  },

  sponsorBanner: {
    position: 'relative',
    top: "90px",
    marginLeft: '40px'   
  },
  sponsorCard: {
    width: '100px', 
    height: '100px',
    left: '10px' 
  }
};


export default props =>

  <Fragment>
    <Paper 
      style={styles.userPageContainer} 
      variant="headline" 
      component="h3"
      square='false'
    >
      <Grid container>
        <Grid item>
          <div style={styles.searchBarContainer}>
            <SearchBar style={{left:"40px", top:"40px"}}/>
          </div>
        </Grid>
        <Grid item>
          <Typography variant='headline' style={styles.sponsorTitle}>Featured Sponsors</Typography>
        </Grid>
        <Grid item style={styles.sponsorBanner}>
          <Card>
            <CardMedia
              image={google_logo}
              title="Google"
              style={styles.sponsorCard}
            />
            <CardContent>
              <Typography gutterBottom variant="headline" component="h2">
                Google Inc.
              </Typography>
            </CardContent>
          </Card>
        </Grid>

        <Grid item style={styles.sponsorBanner}>

          <Card>
            <CardMedia
              image={amazon_logo}
              title="Amazon"
              style={styles.sponsorCard}
            />
            <CardContent>
              <Typography gutterBottom variant="headline" component="h2">
                Amazon
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item style={styles.sponsorBanner}>

          <Card>
            <CardMedia
              image={renaissance_logo}
              title="Renaissance Technologies"
              style={styles.sponsorCard}
            />
            <CardContent>
              <Typography gutterBottom variant="title" component="h2">
                Renaissance
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item style={styles.sponsorBanner}>

          <Card>
            <CardMedia
              image={facebook_logo}
              title="Facebook"
              style={styles.sponsorCard}
            />
            <CardContent>
              <Typography gutterBottom variant="headline" component="h2">
                Facebook
              </Typography>
            </CardContent>
          </Card>          
        </Grid>
        <Grid item style={styles.sponsorBanner}>
          <Card>
            <CardMedia
              image={tesla_logo}
              title="Tesla"
              style={styles.sponsorCard}
            />
            <CardContent>
              <Typography gutterBottom variant="headline" component="h2">
                Tesla
              </Typography>
            </CardContent>
          </Card>          
        </Grid>
      </Grid>
    </Paper>
  </Fragment>




