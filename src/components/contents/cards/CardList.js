import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';

const styles = theme => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    backgroundColor: '#efefef',
    position: 'relative',
    overflow: 'auto',
    maxWidth: 800,
    
    maxHeight: 1000,
    height: 680,
    alignItems: 'center',
    borderWidth: 0,
    marginRight: 0,
    // marginLeft:"33%",
  },
  listItem: {
    align: "center",
    backgroundColor: 'inherit',
  },
  ul: {
    width: '400px',
    backgroundColor: 'inherit',
    padding: 0,
  },
});

function CardList(props) {
  const { classes } = props;
  const nItem = props.numItem;

  return (
    <List className={classes.root} subheader={<li />}>
      {Array(nItem).fill().map((_, i) => i).map(itemId => (
        <li key={`item-${itemId}`}>
          <ul>
            <ListItem key={`item-${itemId}`}>
              <div style={{width: '400px'}}>
                {props.children}                
              </div>
            </ListItem>
          </ul>
        </li>
      ))}
    </List>
  );
}

CardList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CardList);