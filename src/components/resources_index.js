/**
 * The example data is structured as follows:
 *
 * import image from 'path/to/image.jpg';
 * [etc...]
 *
 * const tileData = [
 *   {
 *     img: image,
 *     title: 'Image',
 *     author: 'author',
 *     featured: true,
 *   },
 *   {
 *     [etc...]
 *   },
 * ];
 */

import image from '../images/backgrounds/manhattan-night.jpg';

export const tileData = [
  {
    img: image,
    // title: 'Welcome Page',
    author: 'anonymous',
    featured: true,
  },
];